FILE = bitcnt_1.c bitcnt_2.c bitcnt_3.c bitcnt_4.c main.c bitfiles.c bitstrng.c bstr_i.c rad2deg.c cubic.c isqrt.c

main: ${FILE} Makefile
	gcc -static ${FILE} -O3 -o small_bench -lm

elf: ${FILE} Makefile
	/data/bin/powerpc-elf-gcc -static ${FILE} -O3 -o small_bench.elf /data/powerpc-elf/lib/crt0.o -lm -lc -lsim -msoft-float

clean:
	rm -rf small_bench small_bench.elf

