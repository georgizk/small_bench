#include <stdio.h>
#include <stdlib.h>
#include "conio.h"
#include <limits.h>
#include <time.h>
#include <float.h>
#include "bitops.h"
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <fcntl.h>
#include "snipmath.h"
#include <math.h>
#include <string.h>

#define UNLIMIT
#define MAXARRAY 60000 /* this number, if too large, will cause a seg. fault!! */
#define FUNCS  7

static int CDECL bit_shifter(long int x);
static int bitcount_bench(int iterations);
static int basicmath_bench();
static int qsort_bench();

struct myStringStruct {
  char qstring[128];
};

int compare(const void *elem1, const void *elem2)
{
  int result;

  result = strcmp((*((struct myStringStruct *)elem1)).qstring, (*((struct myStringStruct *)elem2)).qstring);

  return (result < 0) ? 1 : ((result == 0) ? 0 : -1);
}

int main (int argc, char* argv) {
  bitcount_bench(20);
  basicmath_bench();
  qsort_bench();
}

static int bitcount_bench(int iterations)
{  
  int i;
  long j, n, seed;
  static int (* CDECL pBitCntFunc[FUNCS])(long) = {
    bit_count,
    bitcount,
    ntbl_bitcnt,
    ntbl_bitcount,
    /*            btbl_bitcnt, DOESNT WORK*/
    BW_btbl_bitcount,
    AR_btbl_bitcount,
    bit_shifter
  };

  /* initialize random seed: */
  //int randomData = open("/dev/urandom", O_RDONLY);
  //unsigned int myRandomInteger;
  //read(randomData, &myRandomInteger, sizeof myRandomInteger);
  //close (randomData);
  //srand (myRandomInteger);

  // actially, don't want it to be random... we're inserting faults, after all  
  srand (0);

  for (i = 0; i < FUNCS; i++) {
    for (j = n = 0, seed = rand(); j < iterations; j++, seed += 13) {
         n += pBitCntFunc[i](seed);    
    }   
  }
  return 0;
}

static int basicmath_bench() {
  double  a1 = 1.0, b1 = -10.5, c1 = 32.0, d1 = -30.0;
  double  a2 = 1.0, b2 = -4.5, c2 = 17.0, d2 = -30.0;
  double  x[3];
  double X;
  int     solutions;
  int i;
  unsigned long l = 0x3fed0169L;
  struct int_sqrt q;
  long n = 0;

  SolveCubic(a1, b1, c1, d1, &solutions, x);  
  SolveCubic(a2, b2, c2, d2, &solutions, x);

  for (i = 0; i < 101; i+=25)
  {
    usqrt(i, &q);
  }
  usqrt(l, &q);

  for (X = 0.0; X <= 360.0; X += 60.0)
    deg2rad(X);
  for (X = 0.0; X <= (2 * PI + 1e-6); X += (PI / 3))
   rad2deg(X); 
  return 0;
}

static int qsort_bench() {
  struct myStringStruct array[MAXARRAY];
  FILE *fp;
  int i,count=0;

  fp = fopen("input_small_bench.dat","r");
  while((fscanf(fp, "%s", &array[count].qstring) == 1) && (count < MAXARRAY)) {
    count++;
  }
  qsort(array,count,sizeof(struct myStringStruct),compare);

  return 0;
}
static int CDECL bit_shifter(long int x)
{
  int i, n;

  for (i = n = 0; x && (i < (sizeof(long) * CHAR_BIT)); ++i, x >>= 1)
    n += (int)(x & 1L);
  return n;
}

